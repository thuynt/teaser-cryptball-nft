/*!
 * thuynt
 * 
 * 
 * @author thuynt
 * @version 2.0.0
 * Copyright 2021. MIT licensed.
 */$(document).ready(function () {
  $(".icon-menu").click(function () {
    $(".lst-menu").toggleClass("active");
    $(this).toggleClass("close");
  });
  $("#ranger-power").ionRangeSlider({
    min: 0,
    max: 100,
    from: 50
  });
  $("#ranger-accurate").ionRangeSlider({
    min: 0,
    max: 100,
    from: 50
  });
  $("#ranger-spin").ionRangeSlider({
    min: 0,
    max: 100,
    from: 50
  });
  $("#ranger-duration").ionRangeSlider({
    min: 0,
    max: 100,
    from: 50
  });
  $("#ranger-reliability").ionRangeSlider({
    min: 0,
    max: 100,
    from: 50
  });
});
$(function () {
  set_($('#number-max'), 100);

  function set_(_this, max) {
    var block = _this.parent();

    block.find(".increase").click(function () {
      var currentVal = parseInt(_this.val());

      if (currentVal != NaN && currentVal + 1 <= max) {
        _this.val(currentVal + 1);
      }
    });
    block.find(".decrease").click(function () {
      var currentVal = parseInt(_this.val());

      if (currentVal != NaN && currentVal != 0) {
        _this.val(currentVal - 1);
      }
    });
  }
});